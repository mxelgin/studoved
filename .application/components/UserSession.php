<?

$u = new UserSession();
function u()
{
	global $u;
	return $u;
}

class UserSession {

	const CACHE_DURATION = 18000;		// 5 hours
	const COOKIE_NAME = 'l';
	const COOKIE_DURATION = 2592000;	// 30 days

	public $__sid;
	public $id;
	public $role;
	public $avatar;
	public $name;
	public $tz;

	function UserSession()
	{
		// session ID в строке запроса имеет более высокий приоритет, так как флеш использует старые значения куков
		// (которые были на момент инициализации флеша)

		if (@$_GET['sid'] && strstr($_SERVER['HTTP_USER_AGENT'], 'Flash')) {
			$this->__sid = @$_GET['sid'];
		}
		else {
			$this->__sid = @$_COOKIE[self::COOKIE_NAME];
		}

		// auto-load user
		$this->load();

		// setup default timezone
		date_default_timezone_set($this->tz);
	}

	// авторизация текущего юзера:
	// функция вызывается из контроллера при регистрации и авторизации
	// функция вызывается прозрачно для пользователя, когда авторизация происходит по старой сохранившейся куке.
	// todo: сбрасывать куку, если произошла смена страны и браузера. При изменении только страны сброс делать не нужно

	public function authorize($u, $isCookie = false)
	{
		// add entry to log 
		$u->add_login_history($u->user_id, $isCookie ? "cookie" : "site");

		// add cookie
		$this->saveToCookie($u->user_id, $u->passwd);

		//
		$this->update($u);
	}

	public function update($u)
	{
		// save info to session
		// setup current info
		$this->save_session($u);
	}

	public function logout()
	{
		$this->set_cookie('', 0);
	}



	protected function load()
	{
		// default values
		$this->id = 0;
		$this->role = 'guest';
		$this->tz = "Europe/Prague";

		// try to load session data
		if ($this->load_session()) return;

		// try to auto-login from cookie
		if ($this->restoreFromCookie()) return;
	}


	protected function save_session($u)
	{
		$this->id = $u->user_id;
		$this->role = $u->type;
		$this->name = $u->name;
		$this->avatar = $u->avatar;
		$data = array(
						'id' => $this->id,
						'role' => $this->role,
						'name' => $this->name,
						'avatar' => $this->avatar,
					);
		$this->set($this->__sid, $data);
	}


	protected function load_session()
	{
		$str = $this->get($this->__sid);
		if (!is_array($str)) return false;

		$this->id = @$str['id'];
		$this->role = @$str['role'];
		$this->name = @$str['name'];
		$this->avatar = @$str['avatar'];

		return true;
	}



	protected function saveToCookie($id, $passwd_hash)
	{
		$passwd = convert_base(sha1($GLOBALS['config']['cookie']['salt1'] . $passwd_hash. $id), 16, 62);
		$hash = convert_base(strrev(md5($GLOBALS['config']['cookie']['salt2'] . $id . $passwd)), 16, 62);

		$this->set_cookie($id . "." . $hash . "." . $passwd, time_get() + self::COOKIE_DURATION);
	}

	protected function restoreFromCookie()
	{
		$cookie = explode(".", $this->__sid);
		if (count($cookie) != 3) return false;

		list($id, $hash, $passwd) = $cookie;
//        if ($GLOBALS['config']['cookie']['salt2'] )
		// check hash
		if ($hash !== convert_base(strrev(md5($GLOBALS['config']['cookie']['salt2'] . $id . $passwd)), 16, 62)) return false;

		// load user info
		$record = UserModel::findById($id);
		if (!$record) return false;

		// check passwd (it helps find cookie with old passwd)
		if ($passwd != convert_base(sha1($GLOBALS['config']['cookie']['salt1'] . $record->passwd . $id), 16, 62)) return false;

		//
		$this->authorize($record, true);

		return true;
	}







	// функции - обертки
	protected function set_cookie($value, $expire)
	{
		$this->__sid = $value;
		$domain = $GLOBALS['config']['cookie']['domain'];

		setcookie(self::COOKIE_NAME, $value, $expire, "/", $domain, false, true);
	}

	protected function get($key)
	{
		$success = false;
		return apc_fetch($key, $success);
	}

	protected function set($key, $val)
	{
		apc_store($key, $val, self::CACHE_DURATION);
	}

};

