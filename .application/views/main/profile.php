        <section id="middle">
            <div id="content">
                <div class="profile-topBlock clearfix">
                    <div class="userpickBox">
                        <div class="userpick">
                            <img src="media/user1.png" alt="">
                        </div>
                        <div class="attitudeRate clearfix">
                            <button class="A-minus"></button>
                            <div class="attitudeRate-val">409</div>
                            <button class="A-plus"></button>
                        </div>
                    </div>
                    <div class="infoBox">
                        <div class="basicInfo">
                            <div class="userName">McGenley <span class="role">Student</span></div>
                            <div class="userLocation">Hamburg, Germany</div>
                            <div class="userEmail">mcgenley@email.com</div>
                            <div class="userAge">24</div>
                        </div> 
                        <div class="stats clearfix">
                            <div class="statBox first">
                                <span>123</span> days <br> spent
                            </div>
                            <div class="statBox">
                                <span>409</span> lessons <br> complited
                            </div>
                            <div class="statBox last">
                                <span>94</span> subscribers
                            </div>
                        </div>
                    </div>
                    <div class="profileButtons">
                        <button class="btn follow btn-green">Follow</button>
                        <button class="btn message btn-blue">Message</button>
                        <button class="btn testimonial btn-blue">Testimonial</button>
                        <button class="btn favourites btn-blue">Favourites</button>
                    </div>
                    
                </div>

                <article class="profileSection">    
                    <div class="text-head"><h1>Summary</h1> <ul class="filter"><li><a href="#">Edit</a></li></ul><hr></div>
                    <h2>Biography</h2>
                    <p>Our course provides a sound understanding of core, pure and applied economics. However, 
    while you study economics in considerable depth in this specialised degree, you'll employ ideas and techniques from many other disciplines too - these include history, sociology, mathematics and statistics, and politics. Therefore, our graduates are extremely well-qualified for a wide range of jobs and further courses.</p>
                    <h2>Education</h2>
                    <div class="educationPlace">
                        <div class="educationPlaceName">University of the Sunshine Coast</div>
                        <div class="educationField">Bachelor of Business  Marketing</div>
                        <span class="educationLocation">Berlin, Germany</span>
                        <div class="educationYears">1996 - 2000</div>
                    </div>
                    <div class="educationPlace">
                        <div class="educationPlaceName">University of the Sunshine Coast</div>
                        <div class="educationField">Bachelor of Business  Marketing</div>
                        <span class="educationLocation">Berlin, Germany</span>
                        <div class="educationYears">1996 - 2000</div>
                    </div>
                    <h2>Awards & Certificates</h2>
                    <div class="educationPlace">
                        <div class="educationPlaceName">Best Tutors Awards</div>
                        <div class="educationField">1 won best Sociology Tutor</div>
                        <span class="educationLocation">Berlin, Germany</span>
                        <div class="educationYears">2004</div>
                    </div>
                </article>
                <article class="profileSection">
                    <div class="text-head"><h1>Providing courses</h1> <hr></div>
                    <div class="courseBox">
                        <div class="courseImage"><img src="media/BioCourse.jpg" alt=""></div>
                        <div class="courseText">
                            <a href="#" class="courseName">Biology</a>
                            122 students
                        </div>
                    </div>
                    <div class="courseBox">
                        <div class="courseImage"><img src="media/MathCourse.jpg" alt=""></div>
                        <div class="courseText">
                            <a href="#" class="courseName">Mathematics</a>
                            68 students
                        </div>
                    </div>
                    <div class="courseBox">
                        <div class="courseImage"><img src="media/ChemistryCourse.jpg" alt=""></div>
                        <div class="courseText">
                            <a href="#" class="courseName">Chemistry Basics</a>
                            68 students
                        </div>
                    </div>
                </article>
                <article class="profileSection">
                    <div class="text-head"><h1>Taking courses</h1> <hr></div>
                    <div class="courseBox">
                        <div class="courseImage"><img src="media/BioCourse.jpg" alt=""></div>
                        <div class="courseText">
                            <a href="#" class="courseName">Biology</a>
                            <a href="#"><img src="media/teacher1.jpg" alt=""> Mark Levit Ph.D</a> 
                        </div>
                    </div>
                    <div class="courseBox">
                        <div class="courseImage"><img src="media/MathCourse.jpg" alt=""></div>
                        <div class="courseText">
                            <a href="#" class="courseName">Mathematics</a>
                            <a href="#"><img src="media/teacher1.jpg" alt=""> Mark Levit Ph.D</a>
                        </div>
                    </div>
                </article>
                <article class="profileSection">
                    <div class="text-head clearfix"><h1>Followers</h1> 
                        <ul class="filter">
                            <li><a href="#" class="">288 people</a></li>
                        </ul>
                        <hr>
                    </div>
                    <div class="courseBox">
                        <div class="courseImage"><img src="media/userAvatar1.jpg" alt=""></div>
                        <div class="courseText">
                            <a href="#">Jenna Brown</a> 
                        </div>
                    </div>
                    <div class="courseBox">
                        <div class="courseImage"><img src="media/userAvatar2.jpg" alt=""></div>
                        <div class="courseText">
                            <a href="#">Jenna Brown</a> 
                        </div>
                    </div>
                    <div class="courseBox">
                        <div class="courseImage"><img src="media/userAvatar3.jpg" alt=""></div>
                        <div class="courseText">
                            <a href="#">Jenna Brown</a> 
                        </div>
                    </div>
                    <div class="courseBox">
                        <div class="courseImage"><img src="media/userAvatar4.jpg" alt=""></div>
                        <div class="courseText">
                            <a href="#">Jenna Brown</a> 
                        </div>
                    </div>
                    <div class="courseBox">
                        <div class="courseImage"><img src="media/userAvatar1.jpg" alt=""></div>
                        <div class="courseText">
                            <a href="#">Jenna Brown</a> 
                        </div>
                    </div>
                    <div class="courseBox">
                        <div class="courseImage"><img src="media/userAvatar2.jpg" alt=""></div>
                        <div class="courseText">
                            <a href="#">Jenna Brown</a> 
                        </div>
                    </div>
                    <div class="courseBox">
                        <div class="courseImage"><img src="media/userAvatar3.jpg" alt=""></div>
                        <div class="courseText">
                            <a href="#">Jenna Brown</a> 
                        </div>
                    </div>
                    <div class="courseBox">
                        <div class="courseImage"><img src="media/userAvatar4.jpg" alt=""></div>
                        <div class="courseText">
                            <a href="#">Jenna Brown</a> 
                        </div>
                    </div>
                </article>
            </div><!-- /content-->
        </section><!-- /middle-->
<? print_r(u()); ?>