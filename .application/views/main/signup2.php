<script type="text/javascript">

    function addEducation(){
        $('<div/>',{'id':'educationGroup'}).load('/addEdu_ajax', {'type':'0'}, function(){
            $('select').selectBox({
                menuTransition: 'fade',
                menuSpeed: 'fast'
            });
        }).appendTo('div#educations');
    }
   function addCertificate(){
        $('<div/>',{'id':'certificateGroup'}).load('/addEdu_ajax', {'type':'1'}, function(){
            $('select').selectBox({
                menuTransition: 'fade',
                menuSpeed: 'fast'
            });
        }).appendTo('div#certificates');
    }

    function ajaxFileUpload(output){
//        $("#loading")
//            .ajaxStart(function(){
//                $(this).show();
//            })
//            .ajaxComplete(function(){
//                $(this).hide();
//            });

        $.ajaxFileUpload
        (
            {
                url:'/signup2_ajax',
                secureuri:false,
                fileElementId:'fileUpload',
                dataType: 'json',
                success: function (data, status)
                {
// TODO сделать сохранение картинки прямо в тег img
                    $('#picture').val(data.picture);
                    output.empty();
                    var span = $('<span>');
                    $('<img>', {class:'thumb', src:data.picture}).appendTo(span);
                    span.appendTo(output);
                },
                error: function (data, status, e)
                {
                    alert(e);
                }
            }
        )
        return false;

    }

    function onChangePicture(field){
// Check for the various File API support.

//    if (window.File && window.FileReader && window.FileList && window.Blob) {
//        // Great success! All the File APIs are supported.
//        handleFileSelect(field, $('#list'));
//    } else
        {
            // Обработка стандартным способом - загрузка файла на сервер в папку темп, отображение
            ajaxFileUpload($('#list'));
//        alert('The File APIs are not fully supported in this browser.');
        }
    }
</script>

<style>
   /* TODO перенести в css*/
    .thumb {
        height: 75px;
        border: 1px solid #000;
        margin: 10px 5px 0 0;
    }
</style>
        <section id="middle">
            <div id="content" class="clearfix">
              <div class="mainCol register">
                <!-- div class="signInVia">
                    <span>Sign in via</span>
                    <a href="#" class="si-vs facebook"></a>
                    <a href="#" class="si-vs google"></a>
                </div -->
                <div class="StepLabels clearfix">
                    <div class="StepLabel">Step 1. General information</div>
                    <div class="StepLabel active">Step 2. Personal information</div>
                </div>
                <form action="/signup2" method="post" id="registerStep2" class="registerForm clearfix">

                    <div class="clearfix">
                        <div class="control-group left w158">
                            <label class="Label">User picture</label>
                            <div id="res" class="uploadFiles userPicture clearfix">
                                <output id="list">
                                    <div class="uploadFile image"  style="<?=$form->display('picture','false')?>" ></div> <span style="<?=$form->display('picture','false')?>">DRAG YOUR PICTURE HERE</span>
                                    <img class="thumb" src="<?=$form->picture?>" style="<?=$form->display('picture', 'true')?>">
                                </output>
                                <div class="type_file">
                                    Browse
                                    <input id="fileUpload" name="fileUpload" type="file" size="15" class="inputFile" onchange="onChangePicture(this)" value="browse" />
                                    <input id="picture" name="picture" type="hidden" value="<?=$form->picture?>">
                                    <div class="fonTypeFile"></div>
                                </div>
                            </div>
                        </div>
                        <div class="control-group right w463">
                         
                                <label class="Label">Bio</label>
                                <textarea name="bio" id="Bio" placeholder="Short story about yourself , up to 1000 symbols"><?=$form->bio?></textarea>
                    
                        </div>
                    </div>
                    
                    <div class="control-group full">
                        <div id="educations">
                            <label class="Label">Education</label>
                            <?
                                if (is_array($form->type))
                                    for ($id=0; $id<count($form->type); $id++)
                                        if($form->type[$id] == 0)
                                        {
                                            //$divgroup = "div#educationGroup";
                                            echo "<div id='educationGroup'>";
                                            include "_education.php";
                                            echo "</div>";
                                        }
                            ?>
                        </div>
                        <br>
                        <p><button type="button" class="A-plus" onclick="addEducation()"></button></p>
                        <br>

                        <div id="certificates">
                            <label class="Label">Award / Certificates</label>
                            <?
                            if (is_array($form->type))
                                //foreach ($form->type as $key=>$value)
                                for ($id=0; $id<count($form->type); $id++)
                                    if($form->type[$id] == 1)
                                    {
                                        echo "<div id='certificateGroup'>";
                                        include "_education.php";
                                        echo "</div>";
                                    }
                            ?>
                        </div>
                        <br>
                        <p><button type="button" class="A-plus" onclick="addCertificate()"></button></p>
                    </div>
                             

                    <br><br>
                    <div class="clearfix">
                        <div class="control-group left">
                            <p>
                               You can <a href="/id<?=u()->id?>">SKIP THIS TEST</a>.
                            </p>
                        </div>
                        <div class="control-group right clearfix">
                            <button class="btn btn-red w208 h41 m-0">Next</button>
                        </div>
                    </div>                   
                </form>
              </div>
             
            </div><!-- /content-->
        </section><!-- /middle-->
<pre>
<? print_r($form) ?>
</pre>