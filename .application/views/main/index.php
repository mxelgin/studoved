        <div id="promoBlock">
            <div class="slogan">Learning at home!</div>
            <p> Curabitur eget lorem justo, id luctus ante integer aliquam. <br>Facilisis ligula in leo volutpat rutrum. Vestibulum rutrum, libero Sed bibendum velit.</p>
            <button class="btn btn-aqua w196 h43">Register</button>
            <button class="btn btn-green w196 h43">Free Try</button>

        </div>
        
        <div id="benefits">
            <div class="benefits-carouselBox">
                <div class="carouselBox-container">
                    <ul class="carousel">
                        <li >
                            <img src="img/benefit1.png" alt=""/>
                            <div class="benefit">
                                <div class="firstLine">Best teachers</div>
                                The choice is yours
                            </div>
                        </li>
                        <li>
                            <img src="img/benefit2.png" alt=""/>
                            <div class="benefit">
                                <div class="firstLine">Learn together</div>
                                Group lessons
                            </div>
                        </li>
                        <li>
                            <img src="img/benefit3.png" alt=""/>
                            <div class="benefit">
                                <div class="firstLine">Available everywhere</div>
                                From any device
                            </div>
                        </li>
                        <li >
                            <img src="img/benefit2.png" alt=""/>
                            <div class="benefit">
                                <div class="firstLine">Best teachers</div>
                                The choice is yours
                            </div>
                        </li>
                            
                    </ul>
    
                     
                </div>
                   <button class="Cf-prev">Р’Р»РµРІРѕ</button>             
                    <button class="Cf-next">Р’РїСЂР°РІРѕ</button>            
            </div>
        </div>   
        
        <div class="nextCourses">
            <div class="nextCoursesHead">Next Courses</div>

            <div class="nextCoursesList">
                <div class="nextCoursesItem clearfix">
                   
                    <div class="nextCoursesItem-left">
                        <div class="nextCoursesItemHead">Economics</div>
                        <div class="nextCoursesItemTheme">Credit Crisis p7.1.2 Bankruptcy Restructuring Part 1/2</div>
                        <div class="nextCoursesItem-bottom">
                            <span class="nextCoursesItem-date">Wednesday. April, 23, 2013</span>
                            <span class="nextCoursesItem-time">14:30 - 16:30</span>
                            <span class="nextCoursesItem-teacher"><img src="media/teacher1.jpg" alt="">Mark Levit Ph.D</span>
                        </div>

                        <div class="divider"></div>
                    </div>

                    <div class="nextCoursesItem-right">
                        
                        <div class="coursePriceBox clearfix">

                            <div class="coursePrice">
                            $22.50
                            <span>per lesson (2 hours)</span>
                            </div>
                            <button class="btn btn-aqua w159 h24">More</button>
                        </div>
                    </div>
                    
                </div>
                <div class="nextCoursesItem clearfix">
                   
                    <div class="nextCoursesItem-left">
                        <div class="nextCoursesItemHead">Economics</div>
                        <div class="nextCoursesItemTheme">Credit Crisis p7.1.2 Bankruptcy Restructuring Part 1/2</div>
                        <div class="nextCoursesItem-bottom">
                            <span class="nextCoursesItem-date">Wednesday. April, 23, 2013</span>
                            <span class="nextCoursesItem-time">14:30 - 16:30</span>
                            <span class="nextCoursesItem-teacher"><img src="media/teacher1.jpg" alt="">Mark Levit Ph.D</span>
                        </div>

                        <div class="divider"></div>
                    </div>
                    
                    <div class="nextCoursesItem-right">
                        
                        <div class="coursePriceBox clearfix">
                            
                            <div class="coursePrice">
                            $22.50
                            <span>per lesson (2 hours)</span>
                            </div>
                            <button class="btn btn-aqua w159 h24">More</button>
                        </div>
                    </div>
                    
                </div>
                <div class="nextCoursesItem last clearfix">
                   
                    <div class="nextCoursesItem-left">
                        <div class="nextCoursesItemHead">Economics</div>
                        <div class="nextCoursesItemTheme">Credit Crisis p7.1.2 Bankruptcy Restructuring Part 1/2</div>
                        <div class="nextCoursesItem-bottom">
                            <span class="nextCoursesItem-date">Wednesday. April, 23, 2013</span>
                            <span class="nextCoursesItem-time">14:30 - 16:30</span>
                            <span class="nextCoursesItem-teacher"><img src="media/teacher1.jpg" alt="">Mark Levit Ph.D</span>
                        </div>

                        <div class="divider"></div>
                    </div>

                    <div class="nextCoursesItem-right">

                        <div class="coursePriceBox clearfix">
                            
                            <div class="coursePrice">
                            FREE
                            <span>per lesson (2 hours)</span>
                            </div>
                            <button class="btn btn-green w159 h24">Join</button>
                        </div>
                    </div>

                </div>

                <button class="showMore"></button>
            </div>
        </div>
