        <section id="middle">
            <div id="content" class="clearfix">
              <div class="mainCol register">
<!--
                <div class="signInVia">
                    <span>Sign in via</span>
                    <a href="#" class="si-vs facebook"></a>
                    <a href="#" class="si-vs google"></a>
                </div>
-->
                <div class="StepLabels clearfix">
                    <div class="StepLabel active">Step 1. General information</div>
                    <div class="StepLabel">Step 2. Personal information</div>
                </div>
<script>

function onFocusName(field) {
	$("#formName span").hide();
}

function onChangeName(field) {
	$("#formName span").hide();

	if ($.trim(field.value).length < 3)
		$("#formName span").show();
	else
		$("#formName span").hide();
}

function onFocusEmail(field) {
	$("#formEmail span").hide();
}

function onChangeEmail(field) {
	$("#formEmail span").hide();

	var valid = $.trim(field.value).match(/^[a-zA-Z0-9_\-\.\+]+@([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,6}$/);
	if (!valid) {
		$("#formEmail .error.invalid").show();
		return;
	}

	$("#formEmail .wait").show();

	$.post('/signup_ajax', {email: field.value}, function(data) {
		if (!data) return;
		$("#formEmail span").hide();

		if (data.valid)
			$("#formEmail .ok").show();
		else if (data.exists)
			$("#formEmail .error.exists").show();
	});
}

function onChangePasswd1(field) {
	if (field.value.length < 6)
		$('#formPasswd1 .error').show();
	else
		$('#formPasswd1 .error').hide();
}

function onChangePasswd2() {
	if ($('#password').val() != $('#confirmPass').val())
		$('#formPasswd2 .error').show();
	else
		$('#formPasswd2 .error').hide();
}

</script>
                <form action="/signup" method="post" id="registerStep1" class="registerForm">
                    <div class="clearfix">
                        <div class="control-group left">
                            <p id="formEmail">
                                <label for="email">Email</label>
                                <input id="email" name="email" type="text" value="<?=$form->email?>" onchange="onChangeEmail(this)" onfocus="onFocusEmail(this)">
                                <span class="inputCheck ok hidden"></span>
                                <span class="inputCheck wait hidden"></span>
                                <span class="inputCheck error invalid left <?=$form->css('email')?>">Wrong email</span>
                                <span class="inputCheck error exists left <?=$form->css('email', 'unique')?>">Email exists</span>
                            </p>
                        </div>                    
                        <div class="control-group right">
                            <p id="formPasswd1">
                                <label for="password">Password</label>
                                <input id="password" name="passwd" type="password" onchange="onChangePasswd1(this)" onfocus="$('#formPasswd1 span, #formPasswd2 span').hide()">
                                <span class="inputCheck error right <?=$form->css('passwd')?>">6 chars minimum</span>
                            </p>
                            <p id="formPasswd2">
                                <label for="confirmPass">Confirm password</label>
                                <input id="confirmPass" name="passwd2" type="password" onchange="onChangePasswd2()" onfocus="$('#formPasswd2 span').hide();">
                                <span class="inputCheck error right <?=$form->css('passwd2')?>">Not the same</span>
                            </p>
                        </div>
                    </div>
                    <div class="accountType">
                        <span>Account type</span>
                        <div class="right">
                            <label><input type="radio" name="accountType" <?=$form->checked('accountType', 'teacher')?>>Teacher</label>
                            <label><input type="radio" name="accountType" <?=$form->checked('accountType', 'student', true)?>>Student</label>
                        </div>
                    </div> 
                    <div class="clearfix">
                        <div class="control-group left">
                            <p id="formName">
                                <label for="Name">Name</label>
                                <input id="Name" name="name" value="<?=$form->name?>" type="text" onchange="onChangeName(this)" onfocus="onFocusName(this)">
                                <span class="inputCheck error left <?=$form->css('name')?>">3 chars minimum</span>
                            </p>
                            <p>
                                <label for="">Gender</label><br>
                                <label class="forRadio"><input id="male" type="radio" name="gender" <?=$form->checked('gender', 'male')?>>male</label>
                                <label class="forRadio"><input id="female" type="radio" name="gender" <?=$form->checked('gender', 'female')?>>female</label>
                            </p>

                        </div>                    
                        <div class="control-group right">
                            <p>
                                <label for="Phone">Phone</label>
                                <input id="Phone" type="text" name="phone" value="<?=$form->phone?>" placeholder="">
                            </p>
                            <p>
                                <label for="">Date of Birth</label><br>
                                <select name="month" id="month" class="BirthDate">
                                    <option></option>
									<? foreach ($GLOBALS['month'] as $idx => $name): ?>
	                                    <option <?=$form->selected('month', $idx)?>><?=$name?></option>
									<? endforeach; ?>
                                </select>
                                <select name="day" id="date" class="BirthDate">
                                    <option></option>
									<? for ($idx=1; $idx<=31; $idx++): ?>
	                                    <option <?=$form->selected('day', $idx)?>><?=$idx?></option>
									<? endfor; ?>
                                </select>
                                <select name="year" id="year" class="BirthDate">
									<? for ($idx=1920; $idx<=2003; $idx++): ?>
	                                    <option <?=$form->selected('year', $idx, $idx==1980)?>><?=$idx?></option>
									<? endfor;?>
                                </select>

                            </p>
                        </div>
                    </div>

                    <br><br>
                    <div class="clearfix">
                        <div class="control-group left">
                            <p>
                                By clicking Sign up free, you agree to <a href="/terms">Studoved terms of service</a>.
                            </p>
                        </div>
                        <div class="control-group right clearfix">
                            <button class="btn btn-red w208 h41">Next</button>
                        </div>
                    </div>                   
                </form>
              </div>
             
            </div><!-- /content-->
        </section><!-- /middle-->