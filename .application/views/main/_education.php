<div class="control-group full">
    <p>
        <label for="NameOfFacility">Name of Facility</label>
        <input name="nameOfFacility[]" id="NameOfFacility" class="w654 mr-10" type="text" placeholder="" value="<?=$form->nameOfFacility[$id]?>">
    </p>
    <p>
        <label for="Country">Country</label>
        <input name="country[]" id="Country" class="w214 mr-10" type="text" placeholder="" value="<?=$form->country[$id]?>">
    </p>
    <p>
        <label for="City">City/Town</label>
        <input name="city[]" id="City" class="w230 mr-10" type="text" placeholder="" value="<?=$form->city[$id]?>">
    </p>
    <p>
        <label for="StudyYears">Study years</label><br>
        <select name="studyYearsFrom[]" id="StudyYearsFrom" class="StudyYears mr-10">
            <option></option>
            <? for ($idx=1920; $idx<=2003; $idx++): ?>
                <option <?=$form->selected('year', $idx, $idx==$form->studyYearsFrom[$id])?>><?=$idx?></option>
            <? endfor;?>
        </select>
        <span class="">-</span>
        <select name="studyYearsTo[]" id="StudyYearsTo" class="StudyYears ml-9">
            <option></option>
            <? for ($idx=1920; $idx<=2003; $idx++): ?>
                <option <?=$form->selected('year', $idx, $idx==$form->studyYearsTo[$id])?>><?=$idx?></option>
            <? endfor;?>
        </select>
    </p>
    <p>
        <label for="Degree">Degree</label><br>
        <select name="degree[]" id="Degree" class="degree w214 mr-10">
            <option></option>
            <? for ($idx=1920; $idx<=2003; $idx++): ?>
                <option <?=$form->selected('year', $idx, $idx==$form->degree[$id])?>><?=$idx?></option>
            <? endfor;?>
        </select>
    </p>
    <p>
        <label for="Grade">Grade</label><br>
        <input name="grade[]" id="Grade" class="w432" type="text" placeholder="" value="<?=$form->grade[$id]?>">
    </p>
    <p>
        <input name="type[]" id="Type" class="w230 mr-10" type="hidden" placeholder="" value="<?=$form->type[$id]?>">
    </p>
    <p class="right"><button type="button" value="" class="A-minus" onclick="$(this).parent().parent().parent().remove();"></button></p>

</div>
