<?php

class E404Controller extends MxController
{
	
	function action_index()
	{
        $exception = isset($_SESSION['exception']) ? unserialize($_SESSION['exception']) : null;
        unset($_SESSION['exception']);
//        print_r(unserialize($_SESSION['exception']));
        $this->view->generate('404_view.php', 'template_view.php', $exception);
 	}

}
