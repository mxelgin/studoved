<?php

class RegisterController extends MxController
{

	function action_index()
	{	
		
		//print_R(Account::find());
		$flag = true;
		$acc  = null;
		if ($_SERVER['REQUEST_METHOD'] == 'GET'){
			throw new Exception("Unsupported property!");
		}
		
		if (!isset($_POST['signIn-Email'])) { $flag = false; }
		if (!isset($_POST['signIn-Password'])) { $flag = false; }
		
		$this->view->generate('register_step2.php', 'template_view.php');
		
	}
	
	function action_registration(){
		if (isset($_SESSION['s_account']))
           unset($_SESSION['s_account']);
		$this->redirect('register','step1');

    }
	
	function action_step1()
	{
		$account = isset($_SESSION['s_account']) ? unserialize($_SESSION['s_account']) : null;

		if (empty($account)){
			$account = new Account();
			$account->fields['accountType'] = 1;
		}

		$_SESSION['s_account'] = serialize($account);
		$this->view->generate('register_step1.php', 'template_view.php',
				array('account'=>$account));
	}

	function action_step2()
	{

		$account = isset($_SESSION['s_account']) ? unserialize($_SESSION['s_account']) : null;
		
		$flag = true;
        if ($account != null)
		    if ($_SERVER['REQUEST_METHOD'] == 'POST'){

                $account_fields = $this->IsInArray($_POST, 'account_');
                foreach ($account_fields as $key=>$value){
                    $account->fields[$key] = $account_fields[$key];
                }

                if (empty($account->fields['accountType']))
                    $account->fields['accountType'] = 1;

//                if (isset($account->fields['year']) AND isset($account->fields['month']) AND isset($account->fields['date'])){
//                    $account->fields['birthDate'] = (new DateTime())
//                        ->setDate(
//                            $account->fields['year'],
//                            $account->fields['month'],
//                            $account->fields['date']
//                        )
//                        ->format('d-m-Y');
//                }

                $flag = $flag == true ? $account->validate($account->fields) : false;

                try {
                    $fields = Account::find(array('email'=>$account->fields['email']));
                    if (isset($fields[0]))
                    {
                        $flag = false;
                        $account->error['email'] = 'Account is exists';
                    }
                }
                catch (Exception $e) {
                    $this->redirect('e404','index', array('exception'=>$e));
                }
    		}
		
		
		$_SESSION['s_account'] = serialize($account);
 		if ($flag){
			$this->view->generate('register_step2.php', 'template_view.php', array('account'=>$account));
 		} else{
 			$this->redirect('register','step1');//,$account->fields);
 		}
	}
	
	public function action_save(){
		
		$account = unserialize($_SESSION['s_account']);

		$flag = true;
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            // по умочанию одно образование
            $account_fields = $this->IsInArray($_POST, 'account_');
            foreach ($account_fields as $key=>$value){
                $account->fields[$key] = $account_fields[$key];
            }
//            $education_fields[] = array();
            $education_fields = $this->IsInArray($_POST, 'education_');
            if (isset($education_fields )){
                $education_fields = $this->SortInArray($education_fields);
                $edu[] = array();
                foreach ($education_fields as $key=> $value){
                    $education  = new Education();
                    $education->fields = $value;
                    $flag = $education->validate($education->fields) == false ? false : $flag;
                    $edu[$key] = $education;
                }
                $account->setEducation($edu);
            }

            if (!empty($_FILES["picture"]["tmp_name"])){
                $imageName = mysql_real_escape_string($_FILES["picture"]["name"]);
                $imageData = mysql_real_escape_string(file_get_contents($_FILES["picture"]["tmp_name"]));
                $imageType = mysql_real_escape_string($_FILES["picture"]["type"]);

                $account->fields['picture'] = !empty($imageName) ? pathinfo($imageName)['extension'] : null;
            }

			if (!$account->validate($account->fields)){
				$flag = false;
			}
//            $flag = false;
            if ($flag)
            try {
                // todo сделать проверку confirmPass на клиентской стороне
                unset($account->fields['confirmPass'] );

                $flag = $account->insert();
			    if ($flag){
                    if (!empty($_FILES["picture"]["tmp_name"])){
                        $image = new Imagick();
                        $image->readimage($_FILES["picture"]["tmp_name"]);
                        $image->thumbnailImage(100, 0);
                        $profile = $GLOBALS["profile_dir"]."/".$account->fields['id'].".".$account->fields['picture'];
                        $image->writeimage($profile);
                    }
                }
			}
            catch (Exception $e){
                $this->redirect('e404','index', array('exception'=>$e));
            }
		}
// TODO сессию s_account после сохранения нужно удалить, она временная
		$_SESSION['s_account'] = serialize($account);
 		if ($flag){
            $_SESSION['profile'] = serialize($account);
  			$this->redirect('profile', 'index');//,	array('account'=>$account));
 		} else{
  			$this->redirect('register', 'step2'); //,		array('account'=>$account));
 		}
		
	}

// TODO выделить функции отдельное место
    function IsInArray($array , $findme)
    {
        $arr = null;
        foreach ($array as $key => $value)
        {
            if (strpos(strtolower($key), strtolower($findme)) !== false ){
                $arr[substr($key, strlen($findme))] = $value;
            }
        }
        return $arr;
    }

// TODO выделить функции отдельное место
    function SortInArray($array){
        foreach($array as $key=>$params){
            foreach($params as $index => $value) {
                $result[$index][$key] = $value;
            }
        }
        return $result;
    }
}