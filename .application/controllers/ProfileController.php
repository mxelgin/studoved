<?php

class ProfileController extends MxController
{
// TODO аккаунт должен передоваться в профиль, вот так array('profile'=>$profile)
	function action_index()
	{	
		$profile = isset($_SESSION['profile']) ? unserialize($_SESSION['profile']) : null;
		if ($profile->isDB()){
			$this->view->generate('profile.php', 'template_view.php', array('account'=>$profile));
		} else{
			$this->redirect('main', 'index');
		}
	}
	
	function action_login(){
		$flag = true;
        $account = null;
        unset($_SESSION['s_account']);

		if ($_SERVER['REQUEST_METHOD'] == 'GET'){
			throw new Exception("Unsupported property!");
		}
// TODO параметры signIn-Email  signIn-Password перевести в читабельные
		if (!isset($_POST['signIn-Email'])) { $flag = false; }
		if (!isset($_POST['signIn-Password'])) { $flag = false; }
		
		if ($flag)	{
			try {
                $fields = Account::find(array('email'=>$_POST['signIn-Email'],'password'=>$_POST['signIn-Password']));
                if (isset($fields))
                    if (isset($fields[0])){
                    unset($fields[0]['password']);
                    $account = new Account();
                    $account->fields = $fields[0];
                    $edu[] = array();
                    $arrFields = Education::find(array('accountId' => $account->fields['id']));
                    foreach ($arrFields as $key=>$value){
                        $education = new Education();
                        $education->fields = $value;
                        $edu[$key] = $education;
                    }
                    $account->setEducation($edu);

                }
            }
            catch (Exception $e) {
                $this->redirect('e404','index', array('exception'=>$e));
            }
		}

        $_SESSION['s_account'] = serialize($account);
        $_SESSION['profile'] = serialize($account);

        if (!empty($account)){
			$this->redirect('profile','index');
		} else
			$this->view->generate('register_step1.php', 'template_view.php', array('account'=>$account));
	}
	
	function action_logout(){
		if (isset($_SESSION['s_account']))
            unset($_SESSION['s_account']);
        if (isset($_SESSION['profile']))
    		unset($_SESSION['profile']);
		$this->redirect('main','index');
	}
}