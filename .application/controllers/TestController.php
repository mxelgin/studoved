<?php
/**
 * Created by JetBrains PhpStorm.
 * User: MAKSIM
 * Date: 16.08.13
 * Time: 13:58
 * To change this template use File | Settings | File Templates.
 */

class TestController extends MxController {

    public function action_index(){
        try
        {

            $education1 = new Education();
            $education1->fields = array('NameOfFacility'=>'NameOfFacility',
                'Country'=>'Russia','City'=>'Tyumen', 'StudyYearsFrom'=>'1990','StudyYearsTo'=>'2001','Degree'=>'2',
                'Grade'=>'Grade');
            $education2 = new Education();
            $education2->fields = array('NameOfFacility'=>'NameOfFacility',
                'Country'=>'Russia1','City'=>'Tyumen1', 'StudyYearsFrom'=>'1991','StudyYearsTo'=>'2002','Degree'=>'3',
                'Grade'=>'Grade1');

        $account = new Account();
        $account->fields = array('name'=>'Jon','email'=>rand().'jon@mail.ru', 'password'=>'rgredgd');
        $account->setEducation(array('1'=>$education1,'2'=>$education2));

        $account->insert();

        $this->view->generate('template/test.php','template_view.php', $account);
        }
        catch (Exception $e) {
            $this->redirect('e404','index', array('exception'=>$e));
        }
    }

    function action_query(){
        $edu = Education::find(array('accountId'=>'70'));
        print_r($edu);
        //$this->view->generate('template/test.php','template_view.php');
    }

}