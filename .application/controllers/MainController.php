<?php

class MainController extends MxController
{
	// проверяем доступность или не доступность страницы для юзера
	public function redirectUser() {
		if (u()->id) $this->redirect("/id" . u()->id);
	}

	public function redirectGuest($to = null) {
		if (!u()->id) $this->redirect('/signup', $to);
	}

	function action_index() {
		$this->redirectUser();
		$this->render('index');
	}

	function action_login() {
		$this->redirectUser();

		if ($this->isPost) {
			$form = (new UserModel())->login($_POST);
			if (!$form) {
				$this->checkRedirectParam();
				return $this->redirect('/id' . u()->id);
			}
		}
		else {
			$form = new MxForm();
		}

		$this->render('login', ['form' => $form]);
	}

	function action_signup() {
		$this->redirectUser();

		if ($this->isPost) {
			$form = (new UserModel())->signup($_POST);
			if (!$form) {
				return $this->redirect('/signup2');
			}
		}
		else {
			$form = new MxForm();
		}

		$this->render('signup', ['form' => $form]);
	}

	function action_signup_ajax() {
		// check values
		$form = new MxForm();
		$form->rule('email', 'checkEmail');

		if (!$form->validate($_POST)) {
			return $this->renderJSON(['valid' => 0]);
		}
		else if ( (new UserModel())->findByEmail($form->email) ) {
			return $this->renderJSON(['exists' => 1]);
		}

		$this->renderJSON(['valid' => 1]);
	}

	function action_signup2() {

        $this->redirectGuest('/');

        if($this->isPost){
            $form = (new UserModel())->signup2($_POST);
            if (!$form){
                return $this->redirect('/profile');
            }
        } else {
//  TODO добавить данные на форму из БД
            $form = new MxForm();
            $usr = UserModel::findById(u()->id);
            $form->value('bio',$usr->bio);
            $form->value('picture',$usr->avatar);
// тестовые данные
//            $form->value('nameOfFacility' ,array(''));
//            $form->value('country' ,array(''));
//            $form->value('city' ,array(''));
//            $form->value('studyYearsFrom' ,array(''));
//            $form->value('studyYearsTo' ,array(''));
//            $form->value('degree' ,array(''));
//            $form->value('grade' ,array(''));
//            $form->value('type' ,array(''));

        }

		$this->render('signup2', ['form' => $form]);
	}

    function action_signup2_ajax(){
        $form = new MxForm();
        $form->rule('tmp_name','checkImage');

        if ($form->validate($_FILES['fileUpload']))
        {
// Пересохраняем изображение чтоб убить php инъекции
            global $config;

            $image = new Imagick();
            $image->readimage($form->tmp_name);
            $image->thumbnailImage($config['image']['size'], 0);

            $pic_name = u()->id . "." . $image->getimageformat();
            $profile = $_SERVER['CONTEXT_DOCUMENT_ROOT']. $config['image']['tmp_dir']. "/" .$pic_name;
            $image->writeimage($profile);
// TODO не работает через renderJSON
            echo json_encode(['picture'=> $config['image']['tmp_dir']. "/" .$pic_name]);
        } else{
            echo json_encode([]);
        }
}

	function action_logout() {
		$this->redirectGuest('/');
		u()->logout();
		$this->redirect("/");
	}

    function action_addEdu_ajax(){
        if ($this->isPost){
            $form = new MxForm();
            $form->value('nameOfFacility' ,array(''));
            $form->value('country' ,array(''));
            $form->value('city' ,array(''));
            $form->value('studyYearsFrom' ,array(''));
            $form->value('studyYearsTo' ,array(''));
            $form->value('degree' ,array(''));
            $form->value('grade' ,array(''));
            $form->value('type' ,array($_POST['type']));
            $this->render('_education', ['form'=>$form, 'id'=>0], null);

        }
    }


	function action_id() {
		$this->render('profile');
	}

}