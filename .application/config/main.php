<?php

$month = array('Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosínec');

$config = array(
	'passwd_salt' => 'LFSBSbkvGBbzJQafjwBF5He--"__l~;Av"-9A_1%87b2_9I7js]B4_9e6__Y-30-14`\'[\~>vtxQvdz5LMYhBNYR43AtZNrKEU',

	'cookie' => array(
		'domain' => '',		// have to be .domain.com
		'salt1' => 'MPX_2-\'F_z/O}9QU7{wh=B{QwCX~a8[JD!q|\y`J5q7HQI]srmbJbQHGk?ANX2BL>wHwD.}Hr_ZG#35nX!YoJV*o9dHn1;Pna=YQ\'}:v7B<c@,LX{~+0?yN_|f"xrMV.',
		'salt2' => 'wbZpXBzVHyB4aSnK6TvSjPbH3mHvYtyp',
	),

	'restore_pass_timeout' => 86400,

	'db' => array(
		'host' => 'localhost',
		'database' => 'studoved',
		'username' => 'studoved',
		'password' => 'studoved',
		'charset' => 'utf8',
	),

    'image' => array (
        'tmp_dir' => '/media/temp',
        'dir' => '/media/img',
        'size' => '100',
    ),
	// 'profile_dir' => dirname($_SERVER['DOCUMENT_ROOT'])."/studoved/media",
);
