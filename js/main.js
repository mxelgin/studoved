// JavaScript Document
// ��� ���������� �� ������������, ����� ���� ������ � �������� ��� ������������ ���� ���, ��� ����� ��������� � (������ �����) ���������� �� �����������

$(document).ready( function(){
	
	$(function(){
		$('input[placeholder], textarea[placeholder]').placeholder();
	});
	
	$('.config.dropDown').click(function() {
		$(this).next('.dropDownBox').fadeToggle('fast');
		return false;
	});

	$('.dropDown').click(function() {
		var dropDownTarget = $(this).attr('dropDownTarget');
		$('#'+dropDownTarget).fadeToggle('fast');
		return false;
	});

	$('.pictureBoxTools a').mouseenter(function() {
			var tip = $(this).attr('title');
			$('.pictureBoxTools a').not($(this)).parent('li').find('.tooltip').remove();
			$('<div class="tooltip"><span></span>'+tip+'</div>').insertBefore($(this)).stop().fadeIn('fast', function() {
			
			$(this).parent().mouseleave(function () {
				$(this).find('.tooltip').fadeOut('fast', function () {
					$(this).remove();
				});
			})

		});
	});
	$('.pictureBoxTools a').click(function() {
		$(this).addClass('active');
		$('.pictureBoxTools a').not($(this)).removeClass('active')
	});



	/* custom scrollBar */
	//$('.scroll-pane').jScrollPane({
	//	autoReinitialise: true
	//});


	/* PIE */
	if (window.PIE) {
		$('.notifications').each(function() {
			PIE.attach(this);
		});
	}

	
});//end ready







// sign in box
function stopPropagation(event){
    event.stopPropagation();
};

function toggleSignInBox() {
	obj = $(".signInBox");

	if (obj.is(":visible")) {
		obj.fadeOut('fast', function() {
			$(document).unbind('click', toggleSignInBox);
		}).unbind('click', stopPropagation);
	} else {
		obj.fadeIn('fast', function() {
			$(document).bind('click', toggleSignInBox);
		}).bind('click', stopPropagation);
		$('.signInBox input:first').focus();
	}
};



// user menu box

function toggleUserMenuBox() {
	obj = $(".UserMenuBox");

	if (obj.is(":visible")) {
		obj.fadeOut('fast', function() {
			$(document).unbind('click', toggleUserMenuBox);
		}).unbind('click', stopPropagation);
	} else {
		obj.fadeIn('fast', function() {
			$(document).bind('click', toggleUserMenuBox);
		}).bind('click', stopPropagation);
	}
};
