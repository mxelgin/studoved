$(document).ready(function() {
	/* datepicker */
	$('#from.datepicker').datepicker({
		dateFormat: 'MM d, yy',
		showOtherMonths: true,
		selectOtherMonths: true,
		showOn: "button",
		buttonImage: "img/calendarIcon.png",
		buttonImageOnly: false,
		onClose: function( selectedDate ) {
			$( "#to" ).datepicker( "option", "minDate", selectedDate );
		}
	});

	$('#to.datepicker').datepicker({
		dateFormat: 'MM d, yy',
		showOtherMonths: true,
		selectOtherMonths: true,
		showOn: "button",
		buttonImage: "img/calendarIcon.png",
		buttonImageOnly: false,
		onClose: function( selectedDate ) {
			$( "#from" ).datepicker( "option", "maxDate", selectedDate );
		}
	});

	$('#openTime.datepicker').datepicker({
		dateFormat: 'MM d, yy',
		showOtherMonths: true,
		selectOtherMonths: true,
		showOn: "button",
		buttonImage: "img/calendarIcon.png",
		buttonImageOnly: false,
		onClose: function( selectedDate ) {
			$( "#deadLine" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$('#deadLine.datepicker').datepicker({
		dateFormat: 'MM d, yy',
		showOtherMonths: true,
		selectOtherMonths: true,
		showOn: "button",
		buttonImage: "img/calendarIcon.png",
		buttonImageOnly: false,
		onClose: function( selectedDate ) {
			$( "#openTime" ).datepicker( "option", "maxDate", selectedDate );
		}
	});

});


